# musicplayer

This project contains a Python 3 file to search for a music file
under a certain directory and play it using sox.  
The program searches under the `$HOME/Music` directory by default.

## Installation and usage

Run the following commands in a terminal:

```sh
git clone https://gitlab.com/jaspergrayfox/musicplayer.git
cd musicplayer
python playfile.py <args>
```

More info on the args:

```plaintext
usage: playfile.py [-h] [--quiet] [--music-dir MUSIC_DIR] query [query ...]

Searches for a music file and plays it

positional arguments:
  query                 Query to help find the music file

options:
  -h, --help            show this help message and exit
  --quiet               Whether to play it more quietly
  --music-dir MUSIC_DIR
                        Absolute path to the music directory
```

If no results show up, a "No matches found." message shows up
and the program exits.  
If exactly one result is found, it is played immediately.  
If more than one result is found, the paths of all results
are shown to the user, and the program prompts the user
to select a result or cancel. Cancelling exits the program.

## Development

This repository comes with a `requirements.txt` file, but the dependencies
are only there for development purposes. The project uses black to format
the code, and mypy for static analysis.

To install the dependencies to a virtualenv on Linux,
run in the project folder:

```sh
python -m venv .venv
source .venv/bin/activate
python -m pip install -r requirements.txt
```

Then, one can run `black -l 80 *.py` to format the code, and
`mypy .` to check the code for type errors.
