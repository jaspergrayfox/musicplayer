# Copyright 2023 Jasper Gray
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import subprocess
import sys


def get_args():
    parser = argparse.ArgumentParser(
        description=("Searches for a music file and plays it")
    )
    parser.add_argument(
        "queryfragments",
        metavar="query",
        type=str,
        nargs="+",
        help="Query to help find the music file",
    )
    parser.add_argument(
        "--quiet", action="store_true", help="Whether to play it more quietly"
    )
    parser.add_argument(
        "--music-dir",
        type=str,
        default="$HOME/Music",
        help="Absolute path to the music directory",
    )
    args = parser.parse_args()
    query = " ".join(args.queryfragments)
    return {"query": query, "quiet": args.quiet, "music_dir": args.music_dir}


def prompt_entry(findResults):
    print("Multiple entries found:")
    for i, res in enumerate(findResults):
        print(f"{i + 1}. {res}")
    print()

    num_results = len(findResults)
    prompt_str = (
        f"Type a number (1 to {num_results}) "
        "to select an entry or 0 to cancel: "
    )
    num = input(prompt_str)
    fail_validation = True
    while fail_validation:
        try:
            num = int(num)
            assert 0 <= num and num <= num_results
            fail_validation = False
        except:
            print(
                "Input was not a number or number was out of range. "
                "Try again."
            )
            num = input(prompt_str)
    entry = findResults[num - 1] if num > 0 else None
    return entry


def play_entry(entry, quiet):
    if quiet:
        subprocess.run(["play", "-v", "0.25", entry])
    else:
        subprocess.run(["play", entry])


def main():
    args = get_args()
    query = args["query"]
    quiet = args["quiet"]
    music_path = os.path.expandvars(args["music_dir"])
    findQuery = subprocess.run(
        ["find", music_path, "-iname", f"*{query}*", "-type", "f"],
        capture_output=True,
        encoding="utf-8",
    )
    findResults = findQuery.stdout.split("\n")[:-1]
    numMatches = len(findResults)
    if numMatches >= 2:
        entry = prompt_entry(findResults)
        if entry is not None:
            play_entry(entry, quiet)
    elif numMatches == 1:
        play_entry(findResults[0], quiet)
    else:
        print("No matches found.")


if __name__ == "__main__":
    try:
        main()
    except:
        sys.exit(0)
